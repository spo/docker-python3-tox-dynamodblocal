# Docker image with Python3 + tox + DynamoDBlocal on Debian

Build/tool image with Python3.5, tox and dynamodb installed and ready to run. (plus other tools like curl and vi)

To start dynamoDBlocal, run the `/root/start-ddb.sh` startup script. Dynamo will start listening on port 8000
